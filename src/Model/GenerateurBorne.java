package Model;

import static java.lang.Math.random;

public class GenerateurBorne implements StrategieDeGenerationDeTemperature {
    private double temperatureMax;
    private double temperatureMin;


    public GenerateurBorne(){
        this.temperatureMax=Double.MAX_VALUE;
        this.temperatureMin=Double.MIN_VALUE;
    }

    public GenerateurBorne(double temperatureMax, double temperatureMin) {
        this.temperatureMax = temperatureMax;
        this.temperatureMin = temperatureMin;
    }


    public double generer()  {
        return Math.random() * ( temperatureMax - temperatureMin ) + temperatureMin;
    }
}

package Model;


import javafx.application.Platform;

public class CapteurBasique extends Capteur {

    private int delai =1000;


    private StrategieDeGenerationDeTemperature  strategieDeGenerationDeTemperature;

    public CapteurBasique(String ID, String nom, StrategieDeGenerationDeTemperature  strategieDeGenerationDeTemperature) {
        super(ID, nom);
        this.strategieDeGenerationDeTemperature=strategieDeGenerationDeTemperature;
        Thread thread = new Thread();
        thread.setDaemon(true);
        thread.start();
    }

    public int getDelai() {
        return delai;
    }

    public void setDelai(int delai) {
        this.delai = delai;
    }

    public StrategieDeGenerationDeTemperature getStrategieDeGenerationDeTemperature() {
        return strategieDeGenerationDeTemperature;
    }

    public void setStrategieDeGenerationDeTemperature(StrategieDeGenerationDeTemperature strategieDeGenerationDeTemperature) {
        this.strategieDeGenerationDeTemperature = strategieDeGenerationDeTemperature;
    }


    public void run() {
        while (true) {
            try {
                Platform.runLater(()->this.setTemperature(strategieDeGenerationDeTemperature.generer()));
                Thread.sleep(this.delai);
            }catch (InterruptedException e){
                break;
            }
        }
    }


}

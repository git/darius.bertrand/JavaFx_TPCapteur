package Model;

public abstract class Capteur extends Observable {

    private final String ID;
    private String nom;
    private double Temperature=0;

    public Capteur(String ID, String nom, double temperature) {
        this.ID = ID;
        this.nom = nom;
        Temperature = temperature;
    }

    public Capteur(String ID,String nom){
        this.ID = ID;
        this.nom =nom;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public double getTemperature() {
        return Temperature;
    }

    public void setTemperature(double temperature) {
        this.Temperature = temperature;
        uptdate();
    }

    public String getID() {
        return ID;
    }


}

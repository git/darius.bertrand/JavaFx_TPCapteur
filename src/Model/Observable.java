package Model;

import java.util.ArrayList;
import java.util.List;

public class Observable implements Observateur{

    public List<Observateur> listeDObservateur = new ArrayList<>();

    public void ajouterObservateur (Observateur observeur){
        listeDObservateur.add(observeur);
    }

    public void supprimerObservateur (Observateur observeur){
        listeDObservateur.remove(observeur);
    }

    public void uptdate() {
        for (Observateur observateur: listeDObservateur)
            observateur.uptdate();
    }
}

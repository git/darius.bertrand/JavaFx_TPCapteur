package Model;

import javafx.application.Platform;

import java.util.HashMap;
import java.util.Map;

public class ZoneDeCapteur extends Capteur {

    private Map<Capteur, Double> lesCapteurs = new HashMap<>();


    public ZoneDeCapteur(String ID, String nom) {
        super(ID, nom);
        Thread t = new Thread();
        t.setDaemon(true);
        t.start();
    }


    public Map<Capteur, Double> getLesCapteurs() {
        return lesCapteurs;
    }

    public void ajoutCapteurALaZone(Capteur capteur, double poid) {
        lesCapteurs.put(capteur, poid);
    }

    public void suppressionCapteurDeLaZone(Capteur capteur) {
        lesCapteurs.remove(capteur);
    }

    public void run() {
        while (true) {
            try {
                Platform.runLater(() -> {
                    if (lesCapteurs.isEmpty()) return;
                    double temperatureMoyenne = 0;
                    int compteur = 0;
                    for (Map.Entry<Capteur, Double> entry : lesCapteurs.entrySet()) {
                        Capteur capteur = entry.getKey();
                        Double poid = entry.getValue();

                        temperatureMoyenne += capteur.getTemperature() * (1 + poid / 10);
                        compteur++;
                    }
                    this.setTemperature(temperatureMoyenne / compteur);
                });
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                break;
            }
        }

    }


}


